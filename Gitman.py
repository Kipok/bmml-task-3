import numpy as np
# from scipy.spatial.distance import cdist as dst
from scipy.special import digamma, gammaln
from matplotlib import pyplot as plt


class DPMixture:
    def __init__(self, X, alpha, a, b):
        self.X = X
        self.alpha = alpha
        self.a = a
        self.b = b
        self.N = X.shape[0]
        self.D = X.shape[1]
        self.T = int(10 * self.alpha * np.log(1 + self.N / self.alpha))

    def init_variables(self):
        # TODO: change to k-means
        #  centers = np.random.choice(self.N, self.T, replace=False)
        #  self.z = np.zeros((self.N, self.T), dtype=np.bool)
        #  self.z[np.arange(self.N), np.argmin(dst(self.X,
        # self.X[centers]), axis=1)] = 1
        self.zp = np.random.rand(self.N, self.T)
        self.zp /= np.sum(self.zp, axis=1)[:, np.newaxis]

    def compute_stats(self):
        E_theta = digamma(self.theta_a) - digamma(self.theta_a + self.theta_b)
        E_1_theta = digamma(self.theta_b) \
            - digamma(self.theta_a + self.theta_b)
        E_v = digamma(self.v_a) - digamma(self.v_a + self.v_b)
        E_1_v = digamma(self.v_b) - digamma(self.v_a + self.v_b)
        return E_theta, E_1_theta, E_v, E_1_v

    def count_L(self):
        # here I don't compute constants in order to improve performance
        # that is why it may be dangerous to compare this values to values
        # of a different algorithm
        E_theta, E_1_theta, E_v, E_1_v = self.compute_stats()
        L_value = (self.a - 1) * np.sum(E_theta) + (self.b - 1) \
            * np.sum(E_1_theta) + (self.alpha - 1) * np.sum(E_1_v)
        L_value += np.sum(self.zp * (self.X.dot(E_theta.T)
                          + (1 - self.X).dot(E_1_theta.T) + E_v
                          + np.hstack((0, np.cumsum(E_1_v)[:-1]))))
        L_value -= np.sum(np.log(self.zp) * self.zp)
        L_value -= np.sum((self.theta_a - 1) * E_theta
                          + (self.theta_b - 1) * E_1_theta
                          - gammaln(self.theta_a) - gammaln(self.theta_b)
                          + gammaln(self.theta_a + self.theta_b))
        L_value -= np.sum((self.v_a - 1) * E_v + (self.v_b - 1) * E_1_v
                          - gammaln(self.v_a) - gammaln(self.v_b)
                          + gammaln(self.v_a + self.v_b))
        return L_value

    def update_qZ(self):
        E_theta, E_1_theta, E_v, E_1_v = self.compute_stats()
        self.zp = self.X.dot(E_theta.T) + (1 - self.X).dot(E_1_theta.T) \
            + E_v + np.hstack((0, np.cumsum(E_1_v)[:-1]))
        self.zp = np.exp(self.zp - np.max(self.zp, axis=1)[:, np.newaxis])
        self.zp /= np.sum(self.zp, axis=1)[:, np.newaxis]

    def update_qO(self):
        self.theta_a = self.a + self.zp.T.dot(self.X)
        self.theta_b = self.b + self.zp.T.dot(1.0 - self.X)

    def update_qV(self):
        sum_n = np.sum(self.zp, axis=0)
        self.v_a = 1 + sum_n
        self.v_b = self.alpha + np.sum(sum_n) - np.cumsum(sum_n)

    def var_inference(self, num_start=1, display=True,
                      max_iter=100, tol_L=1e-4):
        best_params = {}
        for t in range(num_start):
            self.init_variables()
            new_L = -1e9
            if display is True:
                print('Start number', t + 1)
            for i in range(max_iter):
                cur_L = new_L
                if display is True:
                    print('  Iteration No{}, L = {}'.format(i, cur_L))
                self.update_qV()
                self.update_qO()
                self.update_qZ()
                new_L = self.count_L()
                if new_L - cur_L < tol_L:
                    break
                assert(new_L >= cur_L)
            if len(best_params) == 0 or best_params[0] > new_L:
                best_params = [new_L, self.zp, self.theta_a,
                               self.theta_b, self.v_a, self.v_b]
        self.zp, self.theta_a, self.theta_b = best_params[1:4]
        self.v_a, self.v_b = best_params[4:]
        return self

    def var_inference_from_z(self, fixed_z, display=True,
                             max_iter=100, tol_L=1e-4):
        self.zp = fixed_z
        new_L = -1e9
        for i in range(max_iter):
            cur_L = new_L
            if display is True:
                print('  Iteration No{}, L = {}'.format(i, cur_L))
            self.update_qV()
            self.update_qO()
            self.update_qZ()
            new_L = self.count_L()
            if new_L - cur_L < tol_L:
                break
            assert(new_L >= cur_L)
        return self

    def add_sample(self, X):
        self.X = np.vstack((self.X, X))
        self.N = self.X.shape[0]
        self.T = int(10 * self.alpha * np.log(1 + self.N / self.alpha))
        self.var_inference(display=False)

    def compute_clusters(self):
        clusters_idx = np.argmax(self.zp, axis=1)
        clusters = []
        for cl_num in range(self.T):
            if np.sum(clusters_idx == cl_num) != 0:
                clusters += [np.mean(self.X[clusters_idx == cl_num],
                             axis=0)]
        return np.array(clusters)

    def show_clusters(self):
        x = self.compute_clusters()
        sz = x.shape[0]
        print('Number of clusters: ', sz)
        while int(np.sqrt(sz)) * int(np.sqrt(sz)) != sz:
            sz += 1
        if sz != x.shape[0]:
            x = np.vstack((x, np.zeros((sz - x.shape[0], x.shape[1]))))

        sz1, sz2 = int(np.sqrt(x.shape[0])), int(np.sqrt(x.shape[1]))
        xr = x.reshape(sz1, sz1, sz2, sz2)
        sz2 += 1
        xn = np.empty((sz1*sz2, sz1*sz2), dtype=np.float)

        for i in range(sz1):
            for j in range(sz1):
                xn[i*sz2:(i + 1)*sz2, j*sz2:(j + 1)*sz2] = \
                    np.lib.pad(xr[i, j], ((1, 0), (1, 0)),
                               'constant', constant_values=(0.5, 0.5))

        plt.axis('off')
        plt.imshow(xn[1:, 1:], cmap='gray')
